import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [name, setName] = useState('');
    const [starts, setStart] = useState('');
    const [ends, setEnd] = useState('');
    const [description, setDescription] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');

    const handleName = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStart = (event) => {
        const value = event.target.value;
        setStart(value);
    }

    const handleEnd = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const handleDescription = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleMaxAttendees = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const handleMaxPresentations = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const handleLocation = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.description = description;
        data.name = name;
        data.max_presentations = maxPresentations
        data.max_attendees = maxAttendees
        data.starts = starts
        data.ends = ends
        data.location = location;
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);
            setName('');
            setDescription('');
            setMaxAttendees('');
            setMaxPresentations('');
            setEnd('');
            setStart('');
            setLocation('')
            event.target.reset();
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div class="row">
            <div class="offset-3 col-6 row-10">
                <div class="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div class="row">
                            <div class="form-floating mb-3">
                                <input onChange={handleName} placeholder="Name" required type="text" name="name" id="name" class="form-control" />
                                <label for="name">Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-floating mb-3">
                                <textarea onChange={handleDescription}  placeholder="Description" required type="text" name="description" id="description" class="form-control" rows="5" style={{height: '150px'}}></textarea>
                                <label for="description" class="form-label">Description</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-floating mb-3">
                                <input onChange={handleMaxPresentations}  placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations"
                                    class="form-control" />
                                <label for="max_presentations">Max Presentations</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-floating mb-3">
                                <input onChange={handleMaxAttendees}  placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees"
                                    class="form-control" />
                                <label for="max_attendees">Max Attendees</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-floating mb-3">
                                <input onChange={handleStart}  placeholder="Start Date" required type="date" id="starts" name="starts" class="form-control datepicker" />
                                <label for="starts">Start Date - YYYY-MM-DD</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-floating mb-3">
                                <input onChange={handleEnd}  placeholder="End Date" required type="date" id="ends" name="ends" class="form-control datepicker" />
                                <label for="city">End Date - YYYY-MM-DD</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-3">
                                <select onChange={handleLocation}  required id="location" name="location" class="form-select">
                                    <option value="">Choose a location</option>
                                    {locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ConferenceForm