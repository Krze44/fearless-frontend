import React, { useEffect, useState } from 'react';

function AttendForm() {
    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");
    const [conference, setConference] = useState("");
    const [conferences, setConferences] = useState([]);
    const [showSelect, setShowSelect] = useState(false);
    const [showSpinner, setShowSpinner] = useState(true);

    const formTag = document.getElementById("create-attendee-form");
    const success = document.getElementById("success-message");



    const handleFullNameChange = (event) => {
        const value = event.target.value;
        setFullName(value);
    };

    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    };

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = fullName;
        data.email = email;
        data.conference = conference
        const conferenceUrl = "http://localhost:8001/api/attendees/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newAttendee = await response.json();
            console.log(newAttendee);
            formTag.classList.add("d-none");
            success.classList.remove("d-none");
            setFullName('');
            setEmail('');
            setConference('');
            event.target.reset();
            
            
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            // console.log(data.conferences)
            setConferences(data.conferences);
        }
    }



    useEffect(() => {
        fetchData();
        setShowSelect(true);
        setShowSpinner(false);
    }, []);

    return (
        <div className="my-5">
            <div className="row">
                <div className="col col-sm-auto">
                    <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" alt='' src="./logo.svg" />
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-attendee-form">
                                <h1 className="card-title">It's Conference Time!</h1>
                                <p className="mb-3">
                                    Please choose which conference
                                    you'd like to attend.
                                </p>
                                {showSpinner && (
                                    <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                                        <div className="spinner-grow text-secondary" role="status">
                                            <span className="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                )}
                                {showSelect && (
                                    <div className="mb-3">
                                        <select name="conference" id="conference" className="form-select" required onChange={handleConferenceChange}>
                                            <option value="">Choose a conference</option>
                                            {conferences.map(conference => {
                                                return (
                                                    <option key={conference.href} value={conference.href}>
                                                        {conference.name}
                                                    </option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                )}
                                <p className="mb-3">
                                    Now, tell us about yourself.
                                </p>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input required placeholder="Your full name" type="text" id="name"
                                                name="name" className="form-control" onChange={handleFullNameChange}/>
                                            <label htmlFor="name">Your full name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input required placeholder="Your email" type="email" id="email"
                                                name="email" className="form-control" onChange={handleEmailChange} />
                                            <label htmlFor="email">Your email</label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">I'm going!</button>
                            </form>
                            <div className="alert alert-success d-none mb-0" id="success-message">
                                Congratulations! You're all signed up!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AttendForm
