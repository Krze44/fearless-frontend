import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendForm from './AttendForm';
import MainPage from './MainPage';

import { Route, Routes, BrowserRouter} from 'react-router-dom'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container mt-5">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path='attendees' element={<AttendeesList attendees={props.attendees}/>}>
            <Route path='new' element={<AttendForm />} />
          </Route>
          <Route path='locations' >
            <Route path='new' element={<LocationForm />} />
          </Route>
          <Route path='conferences'>
            <Route path='new' element={<ConferenceForm />} />
          </Route>
          <Route path='presentations'>
            <Route path='new'  element={<PresentationForm />} />
          </Route>
        </Routes>
        {/* <LocationForm /> */}
        {/* <AttendeesList attendees={props.attendees} /> */}
        {/* <ConferenceForm /> */}
        {/* <PresentationForm /> */}
        {/* <AttendForm /> */}
      </div>
    </BrowserRouter>
  );
}

export default App;