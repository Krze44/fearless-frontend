function createCard(name, description, pictureUrl, start, end, location) {
    return `
        <div class="g-col-6 .g-col-md-4 ms-3" style="width: 400px;">
            <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">"${name}"</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">"${description}"</p>
                </div>
                <div class="card-footer">
                    <small class ="text-muted">${start} - ${end}</small>
                </div>
            </div>
        </div>
    `;
    }

function displayError() {
    return `
        <div class="alert alert-danger mx-auto " style="width:450px;"  role="alert">Something went wrong when retrieving conferences!</div>
        <div class="alert alert-danger mx-auto " style="width:300px;"  role="alert">Please try again later!</div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error("Response not OK")
            
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const href = details.conference.href
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = new Date(details.conference.starts);
                    const location = details.conference.location.name;
                    const endDate = new Date(details.conference.ends);
                    const formattedStart = startDate.toLocaleDateString("en-US", { month: 'numeric', day: 'numeric', year: 'numeric'});
                    const formattedEnd = endDate.toLocaleDateString("en-US", { month: 'numeric', day: 'numeric', year: 'numeric'});
                    const html = createCard(title, description, pictureUrl, formattedStart, formattedEnd, location);
                    const row = document.querySelector('.row-cols-3');
                    row.innerHTML += html;
                    console.log(details);
                }
            }
            
        }
    } catch (e) {
        console.error("error", e);
        const html = displayError();
        const error = document.querySelector('.error');
        error.innerHTML += html;
    }
    

    
});