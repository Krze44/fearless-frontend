window.addEventListener('DOMContentLoaded', async () => {
    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(conferenceUrl);
        if (response.ok) {
            const data = await response.json();
            const tag = document.getElementById("conference");
            const conferences = data.conferences;
            for (let conference of conferences) {
                const option = document.createElement("option");
                option.value = conference.name;
                option.innerHTML = conference.name;
                option.setAttribute('data-href', conference.href);
                tag.appendChild(option);
                
            }
        }
    } catch (error) {
        console.error("error", error);
    }

    const formTag = document.getElementById('create-presentation-form');
    const select = document.getElementById("conference");
    
    select.addEventListener("change",  event => {
        const index = select.selectedIndex;
        const conferenceOption = select.options[index];
        const presentationHref = `http://localhost:8000${conferenceOption.getAttribute('data-href')}presentations/`;
        formTag.setAttribute("action", presentationHref);
    });

    formTag.addEventListener("submit",  async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData), (key, value) => {
            if (key === 'href') {
                return undefined;
            }
            return value;
        });
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const presentationUrl = formTag.getAttribute("action");
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
        }
    })
});