window.addEventListener('DOMContentLoaded', async () => {
    const statesUrl = "http://localhost:8000/api/states/";
    try {
        const response = await fetch(statesUrl);
        if (response.ok) {
            const data = await response.json();
            const tag = document.getElementById("state");
            const states = data.states;
            for (let state of states) {
                const option = document.createElement("option");
                option.value = state.abbreviation;
                option.innerHTML = state.name;
                tag.appendChild(option);
                
            }
        }
    } catch (error) {
        console.error("error", error);
    }


    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener("submit",  async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
        }
    });
});