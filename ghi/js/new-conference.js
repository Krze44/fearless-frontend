window.addEventListener('DOMContentLoaded', async () => { 
    const locationsUrl = "http://localhost:8000/api/locations/";

    try {
        const response = await fetch(locationsUrl);
        if (response.ok) {
            const data = await response.json();
            const tag = document.getElementById("location");
            const locations = data.locations;
            for (let location of locations) {
                const option = document.createElement("option");
                option.value = location.id;
                option.innerHTML = location.name;
                tag.appendChild(option);
                
            }
        }
    } catch (error) {
        console.error("error", error);
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener("submit",  async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        console.log(json);
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
        }
    });
});