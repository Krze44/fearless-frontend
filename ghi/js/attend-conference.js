window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const success = document.getElementById("success-message");
    const formTag = document.getElementById("create-attendee-form");
    const loadIcon = document.getElementById("loading-conference-spinner");

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
        const option = document.createElement('option');
        const import_href = conference.href;
        option.value = import_href
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
        }
    }

    selectTag.classList.remove("d-none");
    loadIcon.classList.add("d-none");



    formTag.addEventListener("submit", async event => {
        event.preventDefault();
        const attendeeUrl = "http://localhost:8001/api/attendees/"
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            formTag.classList.add("d-none");
            success.classList.remove("d-none");
            const newAttendee = await response.json();
        }
    });
});